package sheridan;

import java.util.*;

public class GuessingGame {
    private int _numberToGuess;
    private int _rounds;
    private Player[] _playerList;

    public GuessingGame()
    {
        //INITIALIZE all the FIELD VARIABLES
        _numberToGuess = -1; //default out of range for game
        _rounds = 0;
        _playerList = new Player[3];
        
        //create the players and put them in the player list
        _playerList[0] = new Player("Larry");
        _playerList[1] = new Player("Curly");
        _playerList[2] = new Player("Moe");
        
    }
    public void startGame(){
        //choose a number
        Random randNumGen = new Random();
        _numberToGuess = randNumGen.nextInt(11);
        //repeat asking players to guess
        boolean winnerNotFound = true;
        while(winnerNotFound){
            for(int iPlayer = 0;iPlayer < _playerList.length;iPlayer++){
                _playerList[iPlayer].play();
        }
        //determine if anybody won
        winnerNotFound = determineWinner();
     }
    }
    private boolean determineWinner(){
        //go through all the players and find out if their guess
        //matches the number to guess
        
        return false;
    }

}

