package sheridan;

public class Player {
    private String _name;
    private int _guess;

public Player(String name)
{
    //Initialize ALL field variables
    _name = name;
    _guess = -1;
}

public String getName()
{
    return _name;
}

public int getGuess()
{
    return _guess;
}

public void play()
{
}
}